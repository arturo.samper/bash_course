#! /bin/bash

DATE=$(date +%F)

rm *.jpg

for i in 1 2 3 4 5 6  7 8
do
    touch file${i}.jpg
    chmod 777 file${i}.jpg
done

for FILE in *.jpg
do 
    mv $FILE ${DATE}-${FILE}
done

