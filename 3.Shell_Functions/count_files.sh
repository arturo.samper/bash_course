#! /bin/bash

NFILES=0
VALOR=0

function number_of_files_globals(){ 
    #ls lista los ficheros y directorios, grep filtra solo los ficheros (comienzan por "-""), wc -l determina el número de ficheros listados y filtrados
    NFILES=$(ls $1 -l | grep "^-"| wc -l) 
}

function number_of_files_locals(){
    local FILES=0
    FILES=$(ls $1 -l | grep "^-"| wc -l)
    echo "$FILES"
}

number_of_files_globals $1
echo "Number of files : $NFILES"

VALOR=$(number_of_files_locals $1)
echo "Number of files : $VALOR"
