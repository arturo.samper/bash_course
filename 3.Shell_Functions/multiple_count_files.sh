#! /bin/bash

NFILES=0

function file_count(){ 
    #ls lista los ficheros y directorios, grep filtra solo los ficheros (comienzan por "-""), wc -l determina el número de ficheros listados y filtrados
    NFILES=$(ls $1 -l | grep "^-"| wc -l)
    echo "List of files from $1 :"
    ls $1 -l | grep "^-"
}


for PATHS in $@
 do
    file_count $PATHS
    echo "Total number of files : $NFILES"
 done
