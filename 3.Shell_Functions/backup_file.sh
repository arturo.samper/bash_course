#! /bin/bash
DATE=$(date +%F)
DPATH=$(pwd)
FOLDER="backup"

function backup_file() {
    if [ -f $1 ]
    then
        BACK="/$DPATH/$FOLDER/${DATE}-${1}"
        echo "Backing up $1 to ${BACK}"
        cp $1 $BACK
    else
        #file doesn't exist
        return 1
    fi
}

mkdir $FOLDER
[ -d "${FOLDER}" ] && echo "Folder ${FOLDER} created successfully" || echo "[Error] : Folder doesn't created"


backup_file $1
if [ $? -eq 0 ]
then
    echo "Buckup succeeded!"
else
    echo "Buckup failed!"
    exit 1
fi