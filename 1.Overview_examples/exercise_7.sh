#! /bin/bash

if [ -d "${1}" ]
then
    echo "${1} exist and it is a directory."
elif [ -f "${1}" ]
then
    echo "${1} exist and it is a regular file."
else
    echo "${1} is another type of file."
fi

ls -l ${1}