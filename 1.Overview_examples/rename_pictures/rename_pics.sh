#! /bin/bash

PICTURES=$(ls *jpg)
DATE=$(date +%F)
DPATH=$(pwd)
FOLDER="renamed_pic"

mkdir ${FOLDER}
[ -d"/${FOLDER}" ] && echo "Folder ${FOLDER} created successfully" || echo "[Error] : Folder doesn't created"

if [ -d "$FOLDER" ]
then
    echo "correct syntax"
else
    echo "wrong syntax"
fi

for PICTURE in $PICTURES
do
    echo "Renaming ${PICUTRE} to ${DATE}-${PICTURE}"
    mv ${PICTURE} /${DPATH}/${FOLDER}/${DATE}-${PICTURE}
done