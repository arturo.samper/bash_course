#! /bin/bash
SERVER_NAME=$(hostname)
MY_SHELL="bash"
COLORS="red green blue"
echo "Your are running this script on ${SERVER_NAME}."

if [ ${MY_SHELL} = "bash" ]
then
    echo "You seem to like the bash shell."
elif [ ${MY_SHELL} = "csh" ] 
then
    echo "You are currently working with csh shell."
else
    echo "You are currently working with different shell."
fi

for NUMBER in 0 1 2 3 4 5 
do
    echo "Number : $NUMBER"
done

for COLOR in $COLORS
do 
    echo "Color: $COLOR"
done
