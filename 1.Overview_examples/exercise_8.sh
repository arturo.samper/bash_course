#! /bin/bash

for FILE in $@ 
do
    if [ -d "${FILE}" ]
    then
        echo "${FILE} exist and it is a directory."
    elif [ -f "${FILE}" ]
    then
        echo "${FILE} exist and it is a regular file."
    else
        echo "${FILE} is another type of file."
    fi
    ls -l ${FILE}
done