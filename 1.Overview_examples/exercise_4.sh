#! /bin/bash

FILE="/etc/shadow"

if [ -e "${FILE}" ]
then
    echo "Shadow passwords are enabled."
else
    echo "Shadow file does not exist."
fi

if [ -w "${FILE}" ]
then
    echo "You can write the shadow file."
else
    echo "you are not able to write shadow file."
fi