#! /bin/bash

FOLDERS=("2.Return_codes_and_exit" 
         "3.Shell_Functions"
         "4.Shell_scripts_cheklist_and_templates"
         "5.Wildcards" 
         "7.Case_Statements_and_Logic" 
         "8.Logging" 
         "9.While_Loops" 
         "10.Debugging_your_bash_programs" 
         "11.Data_Manipulation_and_text_transformation_with_Sed" 
         "12.Shell_scripts_used_to_create_this_course" 
         "13.Slides" 
         "14.Bonus_section")
COUNTER=0

for FOLDER in ${FOLDERS[@]} 
do
    if [ -d "${FOLDER}" ]
    then
        echo "Folder ${FOLDER} has already been crated."
    else
        echo "creating ${FOLDER}..."
        mkdir ${FOLDER}
        [ -d"/${FOLDER}" ] && echo "    Folder ${FOLDER} created successfully" || echo "    [Error] : Folder doesn't created"
    fi
    #let COUNTER=COUNTER+1
    COUNTER=$(( COUNTER + 1 ))
done

echo "Created Total Folders : ${COUNTER}."